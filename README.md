## Quick Start

### Run in the local machine
You should have node.js version 8 or above installed

1. **Install the packages**
```bash
  npm i
  ```
2. **Build the bundle.js**
```bash
  npm run build
  ```
3. **Run app with console input**
```bash
  npm start
  ```

### How to run unit test
```bash
  npm test
  ```

### Run commands from a file
Add you commands in commands.txt then
```bash
  npm run start:file
  ```
### Run in a docker container

1. **Run a container**
```bash
  docker-compose up -d
  ```
  This process may take a while if you do not have dependencies installed

2. **Run app**

take commands from console
```bash
  docker exec -it <your-container-name> npm run start
  ```

read commands from a file
```bash
  docker exec -it <your-container-name> npm run start:file
  ```

## Object-Oriented Design

### Class Diagram
![Class Diagram](https://s3-ap-southeast-2.amazonaws.com/assets.github.kennethw/class_diagram.png)

### Steps of Execution

1. The App is initiated with a robot and table.
2. The App subscribe to an input source(console/file).
3. The Command Resolver converts inputs to commands.
4. The App executes the commands by calling robot's methods, the robot needs to make sure in the table area.
 