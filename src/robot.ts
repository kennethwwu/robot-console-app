import { Table } from './table';
import { settings } from './setting';

export interface Robot<T>{
    [key:string]: any;
    place(xAxis:number, yAxis:number, facing:string): T;
    move(): T;
    left(): T;
    right(): T;
    report(): T;
}


class RobotBase{
    constructor(
        protected _table:Table,
        protected _xAxis:number,
        protected _yAxis:number,
        protected _facing:string){
    }

    get xAxis(){
        return this._xAxis;
    }

    get yAxis(){
        return this._yAxis;
    }

    get facing(){
        return this._facing;
    }
}


export class MyRobot extends RobotBase implements Robot<MyRobot>{
    private _isInit:boolean = false;

    constructor(
        table:Table,
        xAxis:number = 0,
        yAxis:number = 0,
        facing:string = 'NORTH'){
            super(table, xAxis, yAxis, facing);
        }

    get isInit(){
        return this._isInit;
    }

    place(xAxis:number, yAxis:number, facing:string){
        if(this._table.onTable({xAxis, yAxis})){
            this.setStatus(xAxis, yAxis, facing);
            this._isInit = true;
        }else{
            console.log(`Robot can NOT be off the table!`);
        }
        return this;
    }

    move(){
        let {_xAxis, _yAxis, _facing} = this;
        _xAxis = _xAxis+settings.moveUnits[_facing].x;
        _yAxis = _yAxis+settings.moveUnits[_facing].y;
        if(this._table.onTable({xAxis: _xAxis, yAxis: _yAxis})){
            this.setStatus(_xAxis,_yAxis);
        }else{
            console.log(`Robot can NOT be off the table!`);
        }
        return this;
    }

    left(){
        // turn 90 degreed anticlockwise
        let index = settings.directions.indexOf(this._facing)-1 < 0?settings.directions.length-1:settings.directions.indexOf(this._facing)-1;
        this._facing = settings.directions[index];
        console.log(`turn LEFT`);
        return this;
    }

    right(){
        // turn 90 degreed clockwise
        let index = settings.directions.indexOf(this._facing)+1 > settings.directions.length-1?0:settings.directions.indexOf(this._facing)+1;
        this._facing = settings.directions[index];
        console.log(`turn RIGHT`);
        return this;
    }

    report(){
        console.log(`--> ${this._xAxis},${this._yAxis},${this._facing}`);
        return this;
    }

    private setStatus(x:number, y:number, facing?:string){
        this._xAxis = x;
        this._yAxis = y;
        if(facing){
            this._facing = facing;
        }
    }
}