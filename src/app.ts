import { Robot, MyRobot } from './robot';
import { Command, CommandResolver } from './command';
import { SquareTable } from './table';
import { settings } from './setting';
import { Subject } from 'rxjs';
import * as fs from 'fs';
import * as path from 'path';
import * as rl from 'readline';

// MyApp class
export class App{
    // private stdin:any;
    constructor(private bot:Robot<MyRobot>){}
    exec(command: Command, cb?:Function){
        let result = this.bot[command.method].apply(this.bot, command.args);

        if(cb){
            cb();
        }
    }

    start(): Subject<any> {
        // check the mode to receive the commands
        const resolver = new Subject<any>();

        // if input from console
        if (settings.mod == 'console') {
            process.stdout.write('Please enter your command following by the ENTER key! >');
            process.openStdin().addListener("data", (d: any) => {
                resolver.next(d);
            });
        }

        // read commands from a file
        if (settings.mod == 'file') {
            const file = fs.createReadStream(path.resolve(__dirname, '../commands.txt'), 'utf8');
            const _rl = rl.createInterface({
                input: file
            });

            _rl.on('line', line => {
                resolver.next(line);
            });
        }
        return resolver;
    }
}

// Instantiated MyApp
const table = new SquareTable(settings.tableDimension.length, settings.tableDimension.width);
const robot = new MyRobot(table)
const myApp = new App(robot);

// subscribe to the Input Stream
const inputStream: Subject<any> = myApp.start();

inputStream.subscribe( (input) => {
    // parse the input to command
    const command = CommandResolver.commandParser(input);
    // check robot postion has been initiated
    if(!robot.isInit && command.method != 'place'&& command){
        console.log(`Please run PLACE command to initiate the position!`);
    }
    // execute the commnd
    if(command)myApp.exec(command);
    if (settings.mod == 'console') {
        process.stdout.write('Please enter your command following by the ENTER key! >');
    }
});
