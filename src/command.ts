import { settings } from './setting';
import { Subject } from 'rxjs';
import * as fs from 'fs';
import * as path from 'path';
import * as rl from 'readline';

export interface Command {
    method: string;
    args?: any[];
}

export abstract class CommandResolver {
    constructor() { }
    /*
    / process the commands
    */
    static commandParser(d: any): any {
        let input: string = d.toString().toLowerCase().trim();
        let outputCommand: any;
        console.log(`you entered: ${input}`);

        // catch place command and arguments
        const regex = /^(place)\s+(\d+),(\d+),(north|east|south|west)$/ig;
        let args: any[] = [];
        if (input.match(regex)) {
            ;
            const match = regex.exec(input);
            if (match != null) {
                input = match[1] + ' ';
                args = [Number(match[2]), Number(match[3]), match[4].toUpperCase()]
            }
        }

        switch (input) {
            case 'place ':
                outputCommand = { method: 'place', args };

                break;
            case 'move':
                outputCommand = { method: 'move' };

                break;
            case 'left':
                outputCommand = { method: 'left' };

                break;
            case 'right':
                outputCommand = { method: 'right' };

                break;
            case 'report':
                outputCommand = { method: 'report' };

                break;
            case 'exit':
                process.exit();
                break;
            default:
                outputCommand = false;
                console.log(`invalid command!`);
        }

        return outputCommand;
    }
}