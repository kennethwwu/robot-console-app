export interface Table{
    onTable(position:{xAxis:number, yAxis:number}):boolean;
}

class TableBase{
    protected _xAxisPoints: number[] = [];
    protected _yAxisPoints: number[] = [];

    get xAxisPoints(){
        return this._xAxisPoints;
    }

    get yAxisPoints(){
        return this._yAxisPoints;
    }

    constructor(x:number = 0, y:number = 0){
        if(x > 0){
            this._xAxisPoints = this.getAxisPoints(x);
        }
        if(y > 0){
            this._yAxisPoints = this.getAxisPoints(y);
        }
    }

    private getAxisPoints(len: number):number[]{
        const axisPoints:number[] = [];
        for(let i = 0; i < len; i++){
            axisPoints.push(i);
        }
        return axisPoints;
    }

}

export class SquareTable extends TableBase implements Table{
    
    constructor(length:number = 0, width:number = 0){
        super(length, width);
    }

    public onTable(position:{xAxis:number, yAxis:number}):boolean{
        // 
        // if this position inside the table range
        // 
        if(this._xAxisPoints.includes(position.xAxis) && this._yAxisPoints.includes(position.yAxis)){
            return true;
        }
        return false;
    }


}