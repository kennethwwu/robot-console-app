import { SquareTable } from '../src/table';
import { settings } from '../src/setting';
import { MyRobot } from '../src/robot';
import { App } from '../src/app'
import { Subject } from 'rxjs';

const table = new SquareTable(settings.tableDimension.length, settings.tableDimension.width);
const robot = new MyRobot(table)
const myApp = new App(robot);
const mockCallback = jest.fn(x => true);

test('should create an instance of App', () => {
    expect(myApp).toBeInstanceOf(App);
});

test('start method return an instance of Subject', () => {
    expect(myApp.start()).toBeInstanceOf(Subject);
});

test('exec is executable method', () => {
    myApp.exec({method: 'move'}, mockCallback)
    expect(mockCallback.mock.results[0].value).toBe(true);
});