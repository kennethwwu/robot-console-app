import { SquareTable } from '../src/table';
import { settings } from '../src/setting';
import { MyRobot } from '../src/robot';

const table = new SquareTable(5, 5);
const robot = new MyRobot(table);

test('one PLACE command should be executed to initiate the robot position', () => {
    robot.move();
    expect(robot.isInit).toBe(false);
    robot.place(0, 2, 'NORTH');
    expect(robot.xAxis).toBe(0);
    expect(robot.yAxis).toBe(2);
    expect(robot.facing).toBe('NORTH');
    expect(robot.isInit).toBe(true);
});

test('place robot at (3, 4) facing WEST', () => {
    robot.place(3, 4, 'WEST');
    expect(robot.xAxis).toBe(3);
    expect(robot.yAxis).toBe(4);
    expect(robot.facing).toBe('WEST');
});

test('robot should move a unit forward in the WEST direction', () => {
    robot.move();
    expect(robot.xAxis).toBe(2);
    expect(robot.yAxis).toBe(4);
    expect(robot.facing).toBe('WEST');
});

test('robot should NOT be off the table', () => {
    robot.move().move().move();
    expect(robot.xAxis).toBe(0);
    expect(robot.yAxis).toBe(4);
    expect(robot.facing).toBe('WEST');
});

test('when turn right robot should be facing NORTH', () => {
    robot.right();
    expect(robot.facing).toBe('NORTH');
});

test('when turn left robot should be facing WEST', () => {
    robot.left();
    expect(robot.facing).toBe('WEST');
});

test('REPORT command should return robot current position', () => {
    let res = robot.report();
    expect(res.xAxis).toBe(0);
    expect(res.yAxis).toBe(4);
    expect(res.facing).toBe('WEST');
});

test('when turn 4 times left robot should be facing WEST', () => {
    robot.left().left().left().left();
    expect(robot.facing).toBe('WEST');
});