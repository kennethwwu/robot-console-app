import { CommandResolver } from '../src/command';

test(`enter 'PLACE 0,3,west' should return {method: 'pace', params:[0,3,'WEST']}`, () => {
    const command = CommandResolver.commandParser(`PLACE 0,3,west`);
    expect(command).toEqual({method: 'place', args:[0,3,'WEST']});
});

test(`enter 'MOVE' should return {method: 'move'}`, () => {
    const command = CommandResolver.commandParser(`MOVE`);
    expect(command).toEqual({method: 'move'});
});

test(`enter 'LEFT' should return {method: 'left'}`, () => {
    const command = CommandResolver.commandParser(`LEFT`);
    expect(command).toEqual({method: 'left'});
});

test(`enter 'RIGHT' should return {method: 'right'}`, () => {
    const command = CommandResolver.commandParser(`RIGHT`);
    expect(command).toEqual({method: 'right'});
});

test(`enter 'jkdsjk' should return false`, () => {
    const command = CommandResolver.commandParser(`jkdsjk`);
    expect(command).toBe(false);
});

