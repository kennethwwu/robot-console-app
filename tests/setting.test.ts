import { settings } from '../src/setting';

test('setting should contain directions property', () => {
    expect(settings.directions).toBeDefined();
});

test('should have 4 directions', () => {
    expect(settings.directions.length).toBe(4);
});

test('setting should contain moveUnits property', () => {
    expect(settings.moveUnits).toBeDefined();
});

test('setting should contain mod property', () => {
    expect(settings.mod).toBeDefined();
});

test('setting should contain tableDimension property', () => {
    expect(settings.tableDimension).toBeDefined();
});