import { SquareTable } from '../src/table';

const table = new SquareTable(5, 5);

test('table should have 5 units width', () => {
    expect(table.xAxisPoints.length).toBe(5);
});

test('table should have 5 units length', () => {
    expect(table.xAxisPoints.length).toBe(5);
});

test('point(3,4) should be on the table', () => {
    expect( table.onTable({xAxis:3, yAxis:4}) ).toBe(true);
});

test('point(4,6) should NOT be on the table', () => {
    expect( table.onTable({xAxis:4, yAxis:6}) ).toBe(false);
});